from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .models import Company

from .serializers import CompanySerializer



@api_view(['GET', ])    
def api_detail_company_view(request):    
    try:  
       companies_data = Company.objects.all()
    except Company.DoesNotExist: 
        return Response(status = status.HTTP_404_NOT_FOUND)     
    if request.method == "GET":
        serializer = CompanySerializer(companies_data, many = True)
        return Response(serializer.data)    
