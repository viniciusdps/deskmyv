from django.urls import path 
from . import views 

from .views import api_detail_company_view
app_name = 'companies'
urlpatterns = [
     path('company-list/',api_detail_company_view,name = "company-list"),
]
