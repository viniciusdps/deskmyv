
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view


from .models import User 
from .serializers import UserSerializer

@api_view(['GET'])
def api_detail_user_view(request): 
    try:
       user_data = User.objects.all()
    except User.DoesNotExist: 
        return Response(status = status.HTTP_404_NOT_FOUND)     
    if request.method == "GET":
        serializer = UserSerializer(user_data, many = True)
        return Response(serializer.data)    