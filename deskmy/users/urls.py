from django.urls import path 
from . import views 


from .views import api_detail_user_view 
app_name = 'users'
urlpatterns = [ 
    path('users-list/',api_detail_user_view,name = "users-list"), 
]