from django.db import models
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager

class User(AbstractBaseUser): 
    email = models.CharField(max_length= 30, unique=True ,null= True)
    password = models.CharField(max_length = 30,null= True)
    username = models.CharField(max_length = 30 , unique= True, null= True)
    firstname = models.CharField(max_length= 30) 
    lastname = models.CharField(max_length = 30)
    role = models.CharField(max_length=200)    
    company = models.CharField(max_length= 200 )
    avatar = models.ImageField (upload_to= 'pic_folder', null = True)   
    USERNAME_FIELD = 'username'
