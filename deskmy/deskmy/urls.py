
from django.contrib import admin
from django.urls import path,include

from django.conf import settings
from django.conf.urls.static import static
from companies import urls 
from users import urls
urlpatterns = [
    path('admin/', admin.site.urls),
    #Rest 
    path('api/deskmy/',include('companies.url','companies_api')),
    path('api/deskmy/',include('users.url','users_api'))
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)